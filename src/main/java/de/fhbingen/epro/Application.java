/**
 * 
 */
package de.fhbingen.epro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

/**
 * 
 * @author Johannes Hiemer.
 *
 */
@SpringBootApplication(scanBasePackages = 
		"de.fhbingen.epro.config"
)
public class Application {
	
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        Assert.notNull(ctx);
    }
    
}
