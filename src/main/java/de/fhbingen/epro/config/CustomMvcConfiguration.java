/**
 * 
 */
package de.fhbingen.epro.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author Johannes Hiemer.
 *
 */
@EnableWebMvc
@ComponentScan(basePackages = "de.fhbingen.epro.web")
public class CustomMvcConfiguration {

}
