package de.cloudscale.web.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author Johannes Hiemer.
 *
 */
@Controller
@RequestMapping(value = "/home")
public class HomeController {
	
	private static final Logger log = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		String hostname = "Not Available";
		log.info("Welcome home! The client locale is {}.", locale);
		
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		model.addAttribute("hostname", hostname);
		
		return "home";
	}
	
	@RequestMapping(value = {"/sample" }, method = RequestMethod.GET)
	public String metrics(Locale locale, Model model) {
		log.info("Welcome metrics! The client locale is {}.", locale);
		
		model.addAttribute("sample", null);
		
		return "sample";
	}
	
	@RequestMapping(value = {"/quirk" }, method = RequestMethod.GET)
	public String quirk(Locale locale, Model model) {
		log.info("Welcome quirk! The client locale is {}.", locale);
		
		throw new UnsupportedOperationException("This is a sample exception");
	}
	
}
